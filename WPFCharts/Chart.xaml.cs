﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFCharts
{
    /// <summary>
    /// Interaction logic for Chart.xaml
    /// </summary>
    public partial class Chart : Window
    {
        private Dictionary<string, Series> seriesMap;
        ChartThread chartThread;

        public Chart(ChartThread chartThread)
        {
            this.chartThread = chartThread;
            InitializeComponent();
            seriesMap = new Dictionary<string, Series>();

            ChartArea area = new ChartArea();
            Legend legend = new Legend();

            area.Name = "ChartArea1";
            this.chart.ChartAreas.Add(area);
 
            legend.Name = "Legend1";
            this.chart.Legends.Add(legend);

            this.chart.Location = new System.Drawing.Point(12, 12);
            this.chart.Name = "chart";

            this.chart.Size = new System.Drawing.Size(384, 296);
            this.chart.TabIndex = 0;
            this.chart.Text = "chart";


            SeriesChartType[] e = (SeriesChartType[])(Enum.GetValues(typeof(SeriesChartType)));
            for (int i = 0; i < e.Length; i++)
            {
                combo1.Items.Add(e[i].ToString());
            }

            ChartColorPalette[] e2 = (ChartColorPalette[])(Enum.GetValues(typeof(ChartColorPalette)));
            for (int i = 0; i < e2.Length; i++)
            {
                combo2.Items.Add(e2[i].ToString());
            }

        }


        public List<Series> getSeries()
        {
            List<Series> series = new List<Series>();

            Dictionary<string, Series>.ValueCollection dict = seriesMap.Values;
            foreach (Series s in dict)
            {
                series.Add(s);
            }

            return series;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            foreach (Series s in getSeries())
            {
                s.ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), (sender as ComboBox).SelectedItem.ToString());

            }

        }

        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            chart.Palette = (ChartColorPalette)Enum.Parse(typeof(ChartColorPalette), (sender as ComboBox).SelectedItem.ToString());
        }

        public Series getSeries(string seriesName)
        {
            Series s = null;
            if (!seriesMap.ContainsKey(seriesName))
            {
                s = new Series(seriesName);
                s.ChartType = SeriesChartType.Line;
                chart.Series.Add(s);
                seriesMap.Add(seriesName, s);
            }
            else
            {
                s = seriesMap[seriesName];
            }
            return s;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            chartThread.getThread().Abort();
        }

    }
}
