﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace WPFCharts
{
    public class ChartThread 
    {
        public Chart chart;
        private Thread thread;
        public Dispatcher dispatcher;

        public ChartThread()
        {
            thread = new Thread(run);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        public void ShowWindow()
        {
            
        }

        public void run()
        {
            chart = new Chart(this);
            chart.Show();
            dispatcher = chart.Dispatcher;
            System.Windows.Threading.Dispatcher.Run();
        }

        public Thread getThread()
        {
            return thread;
        }

    }
}
