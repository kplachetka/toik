﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Threading;

namespace WPFCharts
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ChartController c;
        private int chartId = 1;
        Random r = new Random();

        public MainWindow()
        {
            InitializeComponent();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            c = new ChartController();

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {


            for (int i = 0; i < 100; ++i)
            {
                c.putXY("chart" + chartId, i, r.Next(100));
                Thread.Sleep(10);
            }
            chartId++;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 100; ++i)
            {
                c.putY("chart" + chartId, r.Next(100));
                Thread.Sleep(10);
            }
            chartId++;
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            int i = 0;
           while(true)
            {
                c.putXY("Inf", i, r.Next(100));
                Thread.Sleep(100);
            }
        }

    }
}
