﻿using System;
using System.Collections.Generic;
using WPFCharts;

public class ChartController
{
    private delegate void XYDelegate(String series, double x, double y);
    private delegate void YDelegate(String series, double y);

    private ChartThread thread;


    public ChartController()
    {
        thread = new ChartThread();
    }

    public void putY(int y)
    {
        putY("", y);
    }

    public void putY(String series, double y)
    {
        thread.dispatcher.BeginInvoke(
          System.Windows.Threading.DispatcherPriority.Normal,
          new YDelegate(UpdateUserInterface),
          series, y);

    }

    private void UpdateUserInterface(string series, double y)
    {
        thread.chart.getSeries(series).Points.AddY(y);
    }


    public void putXY(double x, double y)
    {
        putXY("", x, y);

    }

    public void putXY(string series, double x, double y)
    {
        thread.dispatcher.BeginInvoke(
               System.Windows.Threading.DispatcherPriority.Normal,
               new XYDelegate(UpdateUserInterface),
               series, x, y);
    }

    private void UpdateUserInterface(string series, double x, double y)
    {
        thread.chart.getSeries(series).Points.AddXY(x, y);
    }

}
